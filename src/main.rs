extern crate blake3;
extern crate hex;
extern crate mac_address;
extern crate tai64;

// use hex;
use mac_address::get_mac_address;
use tai64::Tai64N;

fn main() {
    let mut mac: [u8; 6] = [3, 0, 0, 0, 0, 0];
    match get_mac_address() {
        Ok(Some(ma)) => {
            // println!("MAC addr = {}", ma);
            // println!("bytes = {}", hex::encode(ma.bytes()));
            mac = ma.bytes();
        }
        Ok(None) => println!("No MAC address found."),
        Err(e) => println!("{:?}", e),
    }
    let t64n = Tai64N::now();
    let time = t64n.to_bytes();
    // println!("Tai64N: {:?}", t64n);
    // println!("Tai64N: {}", hex::encode(t64n.to_bytes()));
    println!("--- Simple IPv6 ULA generator ---");
    println!("Algorithm from RFC slightly adapted, using TAI64N instead of NTP");
    println!("timestamp, EUI-48 instead of EUI-64 and Blake3 instead of SHA1.");
    println!("Using MAC     {}", hex::encode(mac));
    println!("Using TAI64N  {}", hex::encode(time));
    println!("");

    let mut hasher = blake3::Hasher::new();
    hasher.update(&mac);
    hasher.update(&time);
    let hash = hasher.finalize();
    let hashbytes: &[u8; 32] = hash.as_bytes();
    println!("ULA: fd{}:{}:{}::/48", hex::encode(&hashbytes[0..1]), hex::encode(&hashbytes[1..3]), hex::encode(&hashbytes[3..5]));
}
