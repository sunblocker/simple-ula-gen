# Simple IPv6 ULA generator in Rust

Need an IPv6 ULA? Here you go!

## Quick install

```
$ cargo install --git='https://git.envs.net/sunblocker/simple-ula-gen.git' --branch='main'
```

## Tinker with the code

```
$ git clone https://git.envs.net/sunblocker/simple-ula-gen.git
$ cd simple-ula-gen
$ cargo run
```

Example output:

```
$ cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.69s
     Running `target/debug/simple-ula-gen`
--- Simple IPv6 ULA generator ---
Algorithm from RFC slightly adapted, using TAI64N instead of NTP
timestamp, EUI-48 instead of EUI-64 and Blake3 instead of SHA1.
Using MAC     525400123456
Using TAI64N  40000000622bc7bf092625c6

ULA: fd3d:7e88:212c::/48
```

If you only want the ULA without the other output, comment out the
`println!(...)` lines in `src/main.rs`, only leave the last `println!(...)` line
as it is.
